FROM igwn/lalsuite-dev:bookworm

LABEL name="LALSuite Development - Clang Dev"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# add llvm repository
RUN apt-get --assume-yes update \
    && apt-get --assume-yes install \
        curl \
        gpg \
    && curl -sSL https://apt.llvm.org/llvm-snapshot.gpg.key | gpg --dearmor --yes --output /usr/share/keyrings/llvm-snapshot.gpg \
    && echo "deb [signed-by=/usr/share/keyrings/llvm-snapshot.gpg] http://apt.llvm.org/bookworm/ llvm-toolchain-bookworm main" > /etc/apt/sources.list.d/llvm.list \
    && apt-get clean

# install clang
RUN apt-get --assume-yes update && \
    apt-get --assume-yes install clang && \
    apt-get clean
